app.controller('attendanceController', ['$scope', '$rootScope', '$http', '$location', '$window',
             'attendanceService', '$routeParams', 'toastr','userService',
    function($scope, $rootScope, $http, $location, $window, attendanceService, $routeParams, toastr, userService) {

        $scope.attendance = {
            id: "",
            userId : "",
            userIdNumb: "",
            date :"", 
            present: ""
        };

        $scope.date = "";
        $scope.present = "";

        $scope.selectedStaff = function(staff){
            $scope.staff = {};
            $scope.date = "";
            $scope.present = "";
            $scope.staff = staff;
        }

        // for search queries 
        $scope.searchName = "";
        $scope.sarchMonth = "1";
        $scope.searchLeaveTaken= "true"

        function getAllAttendance() {
            $scope.staffList = [];
            attendanceService.getAllAttendance().then(function(data) {
                $scope.staffList = data;
            }, function(error) {
                console.error('Error : ' + error)
            });
        };
        // getAllAttendance();

        $scope.register = function(staff) {

            var dateTime = new Date($scope.date);
            dateTime = moment(dateTime).format("YYYY-MM-DD");

            console.log("month",moment(dateTime).format("MMM"))

            if(!$scope.present) {
                $scope.present = false;
            }
           
            $scope.attendance = {
                userId : staff.userEmailId,
                date : dateTime,
                present : $scope.present
            }

            attendanceService.registerAttendance($scope.attendance).then(function() {
                 toastr.success('Attendance registered Successfully','Success');
            }, function(error) {
                 toastr.warning('Something went wrong', 'Please try again later');
            });
            
            $('#exampleModal').modal('hide')
        }

        $scope.getBySearch = function(userId, month, present){
            $scope.staffList = [];

            var isTrueSet;

            if(userId == undefined){
                userId = "";
            }

            if(present == 'true'){
                isTrueSet = (present == 'true');
            }else{
                isTrueSet =  !(present == 'true')
            }

              attendanceService.getBySearch(userId, month, present).then(function(data) {
                console.log("data after query search: ", data);
                $scope.staffList = data;
            }, function(error) {
                console.error('Error : ' + error)
            });
        }

    }
])