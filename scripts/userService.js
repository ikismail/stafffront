app.factory('userService', function ($http) {
    var BASE_URL = "http://localhost:8080/management"

    var userService = this;

    userService.getAllUser = function () {

        return $http.get(BASE_URL + "/getAllUsers").then(
            function (response) {
                return response.data
            }, function (response) {
                console.log('Error : ' + response.data)
                return response.data
            });
    };

    userService.createUser = function (user) {
        return $http.post(BASE_URL + "/user/register/", user).then(
            function (response) {
                return response.data
            }, function (errResponse) {

                console.log('Error while creating user')
                return response.data
            });
    };

    userService.getUserById = function (userId) {
        return $http.get(BASE_URL + "/user/" + userId).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error GetById' + error)
            })
    };

    userService.getUserByEmailId = function (userId) {
        return $http.get(BASE_URL + "/userbyemail/" + userId).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error GetById' + error)
            })
    };

    userService.updateUser = function (userId, user) {
        return $http.put(BASE_URL + "/user/updateUser/" + userId, user).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error GetById' + error)
            })
    };

    userService.deleteUser = function (userId) {
        return $http['delete'](BASE_URL + "/user/deleteUser/" + userId)
            .then(function (response) {
                return response.status
            }, function () {
                console.log(response.status)
            })
    };

    userService.authenticate = function (user) {
        return $http.post(BASE_URL + "/user/validate/", user).then(
            function (response) {
                return response.data;
            }, null);
    };

    userService.logout = function (user) {

        return $http.put(BASE_URL + '/user/logout/',user).then(
            function (response) {
                return response.data;
            },
            null
        );
    };


    return userService;

});