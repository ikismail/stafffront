app.factory('attendanceService', function ($http) {
    var BASE_URL = "http://localhost:8080/management"

    var attendanceService = this;

    attendanceService.getAttendanceById = function (userId) {
        return $http.get(BASE_URL + "/getAttendanceById/" + userId).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error while getting attendance by id' + error)
            })
    };

    attendanceService.getAllAttendance = function () {

        return $http.get(BASE_URL + "/getAllAttendance").then(
            function (response) {
                return response.data
            }, function (response) {
                console.log('Error : ' + response.data)
                return response.data
            });
    };

    attendanceService.registerAttendance = function (attendance) {
        return $http.post(BASE_URL + "/attendance/saveAttendance/", attendance).then(
            function (response) {
                return response.data
            }, function (errResponse) {

                console.log('Error while registering attendance')
                return response.data
            });
    };



    attendanceService.getBySearch = function(userId, month, present){

        var url = BASE_URL + "/attendance/calculateLeave?"

        if(userId){
            url += "&userId=" + userId;
        }
        if(month != null ){
            url += "&month=" + month;
        }
        if(present != null){
            url += "&isPresent=" + present
        }

        console.log("URL for search: " + url);

        return $http.get(url).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error while getting attendance by id' + error)
            })
    }

    
    return attendanceService;

});