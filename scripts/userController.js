app.controller('userController', ['$scope', '$rootScope', '$http', '$location', '$window', 'userService', '$routeParams', 'toastr', 'letterService',
    function($scope, $rootScope, $http, $location, $window, userService, $routeParams, toastr, letterService) {
        $scope.user = {
            userId: '',
            userName: '',
            userIdNumber: '',
            userEmailId: '',
            userPassword: '',
            department: '',
            basicPay: '',
            conveyancePay: '',
            hraPay: '',
            otherAllowance: '',
            contactnumber: '',
            monthlySalary: '',
            annualSalary: '',
            address1: '',
            address2: '',
            pincode: '',
            role: '',
            leaveTaken:'',
            medicalLeave: '',
            casualLeave: ''
        };
        $scope.users;
        $rootScope.logged = false;

        function getAllUsers() {
            userService.getAllUser().then(function(data) {
                $scope.users = data;
            }, function(error) {
                console.error('Error : ' + error)
            });
        };
        getAllUsers();
       
        $scope.createUser = function(user) {
            userService.createUser(user).then(function() {
                $location.path("/main");
            }, function(error) {
                console.log('Error : ' + error)
            });
        };
       
        $scope.submit = function() {
            {
                $scope.createUser($scope.user);
            }
            $scope.reset();
        };
       
        $scope.getById = function($routeParams) {
            userService.getUserById($routeParams.id).then(function(data) {
                $scope.selectedUser = data;
            }, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            })
        };
       
        $scope.getUserById = function() {
            $scope.getById($routeParams);
        }
       
        $scope.removeUser = function(user) {
            $scope.removingUser = user;
        }
       
        $scope.delete = function(userId) {
            userService.deleteUser(userId).then(function(data) {
                console.log("Deleted Successfully");
            }, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            })
        }
       

        $scope.authenticate = function(user) {
            userService.authenticate(user).then(function(d) {
                $scope.user = d;
                console.log('userErrorCode' + $scope.user.errorCode)
                if ($scope.user.errorCode == '404') {
                    toastr.error('Invalid Credentials', 'Please check your credentials');
                    $scope.user.errorMessage = "Invalid Credentials";
                    $scope.user.emailId = '';
                    $scope.user.password = '';
                } else {
                    toastr.success('Authenticating', 'Please wait!');
                    setTimeout(function() {
                        var currentUserEncode = btoa(JSON.stringify($scope.user));
                        localStorage.setItem("token", currentUserEncode);
                        $scope.isLoggedFun();
                        $location.path("/main")
                        $scope.reset();
                    }, 3000)
                }
            }, function(errResponse) {
                console.error('Error while authenticating User');
                toastr.error('Invalid Credentials', 'Please check your credentials');
                $scope.user.errorMessage = "Invalid Credentials please check your Id or password";
                $scope.user.emailId = '';
                $scope.user.password = '';
            });
        };
       
        $scope.login = function() {
            {
                $scope.authenticate($scope.user);
            }
        }
       
        $scope.isLoggedFun = function() {
            var token = localStorage.getItem("token");
            if (token != null) {
                var data = atob(token);
                $scope.loggedUser = JSON.parse(data);
                return true;
            } else {
                return false;
            }
        }
       
        $scope.logout = function() {
            var loggedoutUserEncode = localStorage.getItem("token");
            $scope.logoutUser = atob(loggedoutUserEncode);
            userService.logout(JSON.parse($scope.logoutUser)).then(function(data) {
                localStorage.removeItem("token");
            });
            $location.path("/login");
            $scope.user.errorMessage = "Logout out Successfully";
            $scope.user.emailId = '';
            $scope.user.password = '';
        }
       
        // Users Letter
        $scope.getLettersByUser = function(userId) {
            letterService.getLettersByUserId(userId).then(function(data) {
                $scope.sentLetters = data;
            }, function(error) {
                console.error('Error : ' + error)
            })
        }

        $scope.totalSalary = function(){
            $scope.user.monthlySalary = +($scope.user.basicPay) + 
                                        +($scope.user.conveyancePay) +  
                                          +($scope.user.hraPay) + 
                                            +($scope.user.otherAllowance);

            $scope.user.annualSalary = 12 * $scope.user.monthlySalary;
        }
       
        $scope.reset = function() {
            $scope.user = {
                userId: '',
                userName: '',
                userIdNumber: '',
                userEmailId: '',
                userPassword: '',
                department: '',
                basicPay: '',
                conveyancePay: '',
                hraPay: '',
                otherAllowance: '',
                contactnumber: '',
                monthlySalary: '',
                address1: '',
                address2: '',
                pincode: '',
                role: '',
                leaveTaken:'',
                medicalLeave: '',
                casualLeave: ''
            };
            $scope.myForm.$setPristine();
        };
    }
])