app.controller('letterController', ['$scope', '$rootScope', '$http', '$location', '$window', 'letterService', '$routeParams', 'toastr',
    function($scope, $rootScope, $http, $location, $window, letterService, $routeParams, toastr) {
        $scope.letter = {
            id: "",
            permissionFrom: "",
            permissionTo: "",
            permissionDateFrom: "",
            permissionDateTo: "",
            status: false,
            responseStatus: false,
            content: ""
        };
        $scope.letters;

        $scope.upComingLetters;

        function getAllLetters() {
            letterService.getAllLetters().then(function(data) {
                $scope.letters = data;
            }, function(error) {
                console.error('Error : ' + error)
            });
        };
        getAllLetters();
       
        function getUpComingLetters() {
            letterService.getUpComingLetters().then(
                function(data){
                    $scope.upComingLetters = data;
                }, function(error){
                    console.error('Error : ' + error)
                })
        }

        getUpComingLetters();


        $scope.createLetter = function(letter) {
            var token = localStorage.getItem("token");
            var data = atob(token);
            $scope.loggedUser = JSON.parse(data);
            $scope.letter.permissionFrom = $scope.loggedUser.userEmailId;
            letterService.createLetter(letter).then(function() {
                toastr.success('Letter Sent Successfully' + 'Sent Successfully');
                $location.path("/main");
            }, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            });
        };

        $scope.getById = function($routeParams) {
            letterService.getLetterById($routeParams.id).then(function(data) {
                $scope.selectedLetter = data;
            }, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            })
        };

        $scope.getUserById = function() {
            $scope.getById($routeParams);
        }

        $scope.delete = function(letterId) {
            letterService.deleteUser(letterId).then(function(data) {}, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            })
        }

        $scope.update = function(letterId, letter) {
            letterService.updateLetter(letterId, letter).then(function(data) {
                toastr.success('Successfully Updated' + 'updated successfully');
            }, function(error) {
                toastr.warning('Something went wrong', 'Please try again later');
            })
        };

        $scope.updateStatus = function(letter, status) {
            letter['responseStatus'] = true;
            letter['status'] = status
            $scope.update(letter['id'], letter);
                location.reload();
            
        }

        $scope.reset = function() {
            $scope.user = {
                id: "",
                permissionFrom: "",
                permissionTo: "",
                permissionDateFrom: "",
                permissionDateTo: "",
                status: false,
                responseStatus: false,
                content: ""
            };
            $scope.myForm.$setPristine();
        };
    }
])