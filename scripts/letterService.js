app.factory('letterService', function ($http) {
    var BASE_URL = "http://localhost:8080/management"

    var letterService = this;

    letterService.getAllLetters = function () {

        return $http.get(BASE_URL + "/getAllLetters").then(
            function (response) {
                return response.data
            }, function (response) {
                console.log('Error : ' + response.data)
                return response.data
            });
    };


    letterService.getUpComingLetters  = function(){
        return $http.get(BASE_URL + "/getUpcomingLetters").then(
            function(response){
                return response.data
            }, function(response){
                console.log('Error : ' + response.error)
                return response.data
            })
    }


    letterService.getLettersByUserId = function(userId){

        return $http.get(BASE_URL + "/getLettersByUser/"+ userId).then(
            function (response) {
                return response.data
            }, function (errResponse) {
                return response.data
            });
    }

    letterService.createLetter = function (letter) {
        return $http.post(BASE_URL + "/letter/createLetter/", letter).then(
            function (response) {
                return response.data
            }, function (errResponse) {

                console.log('Error while creating letter')
                return response.data
            });
    };

    letterService.getLetterById = function (letterId) {
        return $http.get(BASE_URL + "/letter/" + letterId).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error GetById' + error)
            })
    };

    letterService.updateLetter = function (letterId, letter) {
        return $http.put(BASE_URL + "/letter/updateLetter/" + letterId, letter).then(
            function (response) {
                return response.data
            }, function (error) {
                console.error('Error GetById' + error)
            })
    };

    letterService.deleteLetter = function (letterId) {
        return $http['delete'](BASE_URL + "/letter/deleteLetter/" + letterId)
            .then(function (response) {
                return response.status
            }, function () {
                console.log(response.status)
            })
    };

    return letterService;

});