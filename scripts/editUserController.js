app.controller('editUserController', function ($scope, $routeParams, $location,
    userService, toastr) {
    console.log('entering editController')

    var userId = $routeParams.id;

    $scope.user = {
        userId: '',
        userName: '',
        userIdNumber: '',
        userEmailId: '',
        userPassword: '',
        department: '',
        basicPay: '',
        conveyancePay: '',
        hraPay: '',
        otherAllowance: '',
        contactnumber: '',
        monthlySalary: '',
        annualSalary: '',
        address1: '',
        address2: '',
        pincode: '',
        role: '',
        leaveTaken:'',
        medicalLeave: '',
        casualLeave: ''
    };

    $scope.user = userService.getUserById(userId).then(function (data) {
        console.log("edited USer", data);
        $scope.user = data;
    }, null)

    $scope.update = function (user) {
        console.log('entering update function in editController')
        userService.updateUser(userId, user).then(function (data) {
            console.log("Updated Data", data);
            toastr.success('Successfully Updated', data['userName'] + 'updated successfully');
        }, function (error) {
            toastr.warning('Something went wrong', 'Please try again later');
        })
    };

    $scope.totalSalary = function(){
            $scope.user.monthlySalary = +($scope.user.basicPay) + 
                                        +($scope.user.conveyancePay) +  
                                          +($scope.user.hraPay) + 
                                            +($scope.user.otherAllowance);

            $scope.user.annualSalary = 12 * $scope.user.monthlySalary;
        }

})