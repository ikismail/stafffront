var app = angular.module('myApp', ['ngRoute', 'ngCookies', 'ngAnimate', 'toastr', 'ngtimeago']);
app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: './views/login.html'
    }).when('/login', {
        templateUrl: './views/login.html'
    }).when('/main', {
        templateUrl: './views/main.html'
    }).when('/createEmployee', {
        templateUrl: './views/register.html'
    }).when('/profile/:id', {
        templateUrl: './views/profileView.html',
        controller: 'editUserController'
    }).when('/viewEmployee/:id', {
        templateUrl: './views/viewEmployee.html'
    }).when('/updateEmployee/:id', {
        templateUrl: './views/editUser.html',
        controller: 'editUserController'
    }).when('/permissionLetters', {
        templateUrl: './views/letters.html',
        controller: 'letterController'
    }).when('/createLetter', {
        templateUrl: './views/createLetter.html',
        controller: 'letterController'
    }).when('/sentLetters', {
        templateUrl: './views/sentLetters.html'
    }).when('/attendance', {
        templateUrl : './views/attendance.html',
        controller : 'attendanceController'
    }).when('/attendanceverify', {
        templateUrl : './views/attendanceverify.html',
        controller : 'attendanceController'
    }).otherwise({
        redirectTo: '/main'
    })
})

app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});